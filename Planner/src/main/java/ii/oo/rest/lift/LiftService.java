package ii.oo.rest.lift;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import ii.oo.dao.lift.LiftDAO;
import ii.oo.dto.exercise.Lift;
import ii.oo.rest.utils.HibernateUtils;
import ii.oo.rest.utils.ResponseUtil;

@Path("/lift")
public class LiftService {

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			LiftDAO dao = new LiftDAO(session);
			List<Lift> types = dao.getAllLifts();
			return ResponseUtil.buildOKJSONResponse(types);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") int id) {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			LiftDAO dao = new LiftDAO(session);
			Lift type = dao.getLift(id);
			return ResponseUtil.buildOKJSONResponse(type);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

}
