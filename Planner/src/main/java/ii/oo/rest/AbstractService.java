package ii.oo.rest;

import javax.servlet.http.HttpServletRequest;

public abstract class AbstractService {
	
	protected String ip;
	protected String userAgent;
	
	protected void setRemoteHostInfo(HttpServletRequest req) {
		ip = req.getRemoteAddr();
		userAgent = req.getHeader("User-Agent");
	}

}
