package ii.oo.rest.user;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import ii.oo.dao.security.DAOProvider;
import ii.oo.dao.security.SecurityDAOProvider;
import ii.oo.dao.session.SessionDataAccess;
import ii.oo.dao.user.UserDAO;
import ii.oo.dao.user.UserDataAccess;
import ii.oo.dto.session.UserSession;
import ii.oo.dto.user.User;
import ii.oo.dto.user.UserPassword;
import ii.oo.dto.user.login.LoginResponse;
import ii.oo.dto.user.login.LoginStatus;
import ii.oo.flowcontrol.user.UserResponse;
import ii.oo.rest.AbstractService;
import ii.oo.rest.utils.HibernateUtils;
import ii.oo.rest.utils.ResponseUtil;

@Path("/user")
public class UserService extends AbstractService {

	private DAOProvider provider;

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll(@Context HttpServletRequest req) {
		SessionFactory factory = null;
		Session session = null;
		try {
			setRemoteHostInfo(req);
			provider = new SecurityDAOProvider(null);
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			UserDataAccess dao = provider.getUserDAO(session);
			List<User> users = dao.getAllUsers();
			return ResponseUtil.buildOKJSONResponse(users);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") int id) {
		SessionFactory factory = null;
		Session session = null;
		try {
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			UserDAO dao = new UserDAO(session);
			User user = dao.getUser(id);
			return ResponseUtil.buildOKJSONResponse(user);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

	@POST
	@Path("/new")
	@Produces(MediaType.APPLICATION_JSON)
	public Response saveNewUser(@Context HttpServletRequest req, @FormParam("username") String username,
			@FormParam("email") String email, @FormParam("password") String password) {
		SessionFactory factory = null;
		Session session = null;
		Transaction tx = null;
		try {

			// Set remote host info, ip, user agent
			setRemoteHostInfo(req);

			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			tx = session.getTransaction();
			tx.begin();
			
			User user = new User();
			user.setEmail(email);
			user.setUsername(username);

			UserPassword userPassword = new UserPassword();
			userPassword.setPassword(password);
			userPassword.setUser(user);

			// Create a limited session object containing the ip, user agent and partial user
			UserSession limitedSession = new UserSession();
			limitedSession.setIp(ip);
			limitedSession.setUserAgent(userAgent);
			limitedSession.setUser(user);
			// Use the security dao provider with the limited session
			provider = new SecurityDAOProvider(limitedSession);

			UserDataAccess dao = provider.getUserDAO(session);

			UserResponse response = dao.saveNewUser(userPassword);

			tx.commit();
			return ResponseUtil.buildOKJSONResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

	/**
	 * Validate a user login by checking the username and password. Create and
	 * return a new session in the response.
	 * 
	 * @param req
	 * @param username
	 * @param password
	 * @return {@link LoginResponse} with failure or success, success will also
	 *         contain the generated session.
	 */
	@POST
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@Context HttpServletRequest req, @FormParam("username") String username,
			@FormParam("password") String password) {
		SessionFactory factory = null;
		Session session = null;
		Transaction tx = null;
		try {
			// Set remote host info, ip, user agent
			setRemoteHostInfo(req);

			// Create a limited session object containing the ip and user agent
			UserSession limitedSession = new UserSession();
			limitedSession.setIp(ip);
			limitedSession.setUserAgent(userAgent);

			// Use the security dao provider with the limited session
			provider = new SecurityDAOProvider(limitedSession);

			// Setup a hibernate session and transaction
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			tx = session.getTransaction();
			tx.begin();

			// Get the user and session dao from the provider
			UserDataAccess dao = provider.getUserDAO(session);
			SessionDataAccess sessionDAO = provider.getSessionDAO(session);

			// Create user password and validate with the user dao
			User user = new User();
			user.setUsername(username);
			UserPassword userPassword = new UserPassword();
			userPassword.setPassword(password);
			userPassword.setUser(user);
			User login = dao.validateLogin(userPassword);

			// Setup a response to the validation
			LoginResponse response = new LoginResponse();
			response.setStatus(LoginStatus.FAILED);
			if (login != null) {
				// If login validated setup a session for the user
				UserSession userSession = sessionDAO.createUserSession(login, ip, userAgent);
				response.setSessionId(userSession.getSessionId());
				response.setStatus(LoginStatus.SUCCESS);
			}
			return ResponseUtil.buildOKJSONResponse(response);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			tx.commit();
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

}
