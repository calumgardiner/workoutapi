package ii.oo.rest.utils;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class HibernateUtils {

	public static SessionFactory createSessionFactory() throws Exception {
		Configuration configuration = new Configuration();
		configuration.configure();
		StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
				.applySettings(configuration.getProperties()).build();
		SessionFactory sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		return sessionFactory;
	}

	public static void close(SessionFactory sf) {
		try {
			sf.close();
		} catch (Exception e) {
		}
	}

	public static void close(Session s) {
		try {
			s.close();
		} catch (Exception e) {
		}
	}
	
}
