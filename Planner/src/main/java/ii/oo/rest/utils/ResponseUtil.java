package ii.oo.rest.utils;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class ResponseUtil {

	public static Response buildOKJSONResponse(Object object) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();
		String response = gson.toJson(object);
		return Response.status(200).entity(response).build();
	}

}
