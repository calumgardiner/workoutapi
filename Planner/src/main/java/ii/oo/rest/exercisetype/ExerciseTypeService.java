package ii.oo.rest.exercisetype;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import ii.oo.dao.exercisetype.ExerciseTypeDAO;
import ii.oo.dto.exercise.ExerciseType;
import ii.oo.rest.utils.HibernateUtils;
import ii.oo.rest.utils.ResponseUtil;

@Path("/exercise-type")
public class ExerciseTypeService {

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() {
		SessionFactory factory = null;
		Session session = null;
		try{
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			ExerciseTypeDAO typeDao = new ExerciseTypeDAO(session);
			List<ExerciseType> types = typeDao.getAllExerciseTypes();
			return ResponseUtil.buildOKJSONResponse(types);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}
	
	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") int id) {
		SessionFactory factory = null;
		Session session = null;
		try{
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			ExerciseTypeDAO typeDao = new ExerciseTypeDAO(session);
			ExerciseType type = typeDao.getExerciseType(id);
			return ResponseUtil.buildOKJSONResponse(type);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

}
