package ii.oo.rest.exercisegroup;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import ii.oo.dao.exercisegroup.ExerciseGroupDAO;
import ii.oo.dto.exercise.ExerciseGroup;
import ii.oo.rest.utils.HibernateUtils;
import ii.oo.rest.utils.ResponseUtil;

@Path("/exercise-group")
public class ExerciseGroupService {
	
	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public Response listAll() {
		SessionFactory factory = null;
		Session session = null;
		try{
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			ExerciseGroupDAO dao = new ExerciseGroupDAO(session);
			List<ExerciseGroup> groups = dao.getAllExerciseGroups();
			return ResponseUtil.buildOKJSONResponse(groups);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}
	
	@GET
	@Path("/id/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") int id) {
		SessionFactory factory = null;
		Session session = null;
		try{
			factory = HibernateUtils.createSessionFactory();
			session = factory.openSession();
			ExerciseGroupDAO dao = new ExerciseGroupDAO(session);
			ExerciseGroup group = dao.getExerciseGroup(id);
			return ResponseUtil.buildOKJSONResponse(group);
		} catch (Exception e) {
			e.printStackTrace();
			return Response.status(200).entity(e.getMessage()).build();
		} finally {
			HibernateUtils.close(session);
			HibernateUtils.close(factory);
		}
	}

}
