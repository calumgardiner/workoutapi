package ii.oo.flowcontrol.user;

import ii.oo.dto.user.User;
import ii.oo.flowcontrol.Response;

public class UserResponse extends Response {

	private User user;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
