package ii.oo.flowcontrol;

public abstract class Response {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public static <T extends Response> T createOkResponse(Class<T> clazz) throws Exception {
		return createResponseWithStatus(clazz, "ok");
	}
	
	public static <T extends Response> T createResponseWithStatus(Class<T> clazz, String status) throws Exception {
		T response = clazz.newInstance();
		response.setStatus(status);
		return response;
	}
}
