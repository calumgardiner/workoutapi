package ii.oo.dto.exercise;

import java.util.Date;

import ii.oo.dto.user.User;

/**
 * Cardio: <br>
 * timeTaken in ms, distance in metres
 * 
 * @author calum
 *
 */
public class Cardio {

	private Date startTime;
	private Date endTime;
	private long timeTaken;
	private int distance;
	private Exercise exercise;
	private User user;

	public Cardio() {
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}

	public Exercise getExercise() {
		return exercise;
	}

	public void setExercise(Exercise exercise) {
		this.exercise = exercise;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
