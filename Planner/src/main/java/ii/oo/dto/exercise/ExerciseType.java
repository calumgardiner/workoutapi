package ii.oo.dto.exercise;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Exercise Type
 * 
 * @author calum
 *
 */
@Entity
@Table(name = "exercise_type")
public class ExerciseType {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "name", nullable = false, unique = true)
	private String name;

	@Column(name = "description", nullable = false)
	private String description;

	@ManyToOne
	@JoinColumn(name = "exercise_group_id")
	private ExerciseGroup group;

	public ExerciseType(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public ExerciseType() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ExerciseGroup getGroup() {
		return group;
	}

	public void setGroup(ExerciseGroup group) {
		this.group = group;
	}

}
