package ii.oo.dto.exercise;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ii.oo.dto.user.User;

@Entity
@Table(name = "lift")
public class Lift {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "reps", nullable = false)
	private int reps;

	@Column(name = "sets", nullable = false)
	private int sets;

	@ManyToOne
	@JoinColumn(name = "exercise_id")
	private Exercise exercise;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	public Lift() {

	}

	public int getReps() {
		return this.reps;
	}

	public void setReps(int reps) {
		this.reps = reps;
	}

	public int getSets() {
		return this.sets;
	}

	public void setSets(int sets) {
		this.sets = sets;
	}

	public Exercise getExercise() {
		return exercise;
	}

	public void setExercise(Exercise exercise) {
		this.exercise = exercise;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
