package ii.oo.dto.authorization;

public enum AuthorizationStatus {
	
	AUTHORIZED("Authorized"),
	NOT_AUTHORIZED("Not authorized");
	
	private String description;
	
	private AuthorizationStatus(String description) {
		this.description = description;
	}
	
	public String getDescription() {
		return this.description;
	}

}
