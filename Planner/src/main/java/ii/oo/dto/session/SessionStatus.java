package ii.oo.dto.session;

public enum SessionStatus {

	SESSION_EXPIRED("Session expired"), INVALID_SESSION("Invalid session"), VALID_SESSION("Valid session");

	private String description;

	private SessionStatus(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}
