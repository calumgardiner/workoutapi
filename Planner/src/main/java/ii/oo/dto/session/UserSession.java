package ii.oo.dto.session;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ii.oo.dto.user.User;

@Entity
@Table(name = "session")
public class UserSession {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "session_id", nullable = false, unique = true)
	private String sessionId;

	@Column(name = "ip_address", nullable = false)
	private String ip;

	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;

	@Column(name = "user_agent", nullable = false)
	private String userAgent;

	@Column(name = "session_start", nullable = false)
	private Date sessionStart;

	public Date getSessionStart() {
		return sessionStart;
	}

	public void setSessionStart(Date sessionStart) {
		this.sessionStart = sessionStart;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
