package ii.oo.dto.session;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "session_expiry")
public class SessionExpiry {

	@Id
	@Column(name = "expiry", nullable = false)
	private int hoursToExpiry;

	public int getHoursToExpiry() {
		return hoursToExpiry;
	}

	public void setHoursToExpiry(int hoursToExpiry) {
		this.hoursToExpiry = hoursToExpiry;
	}

}
