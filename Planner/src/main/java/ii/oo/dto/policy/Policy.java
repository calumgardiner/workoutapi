package ii.oo.dto.policy;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "policy")
public class Policy {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "name", nullable = false, unique = true)
	private String name;

	@Column(name = "property", nullable = false)
	private int property;

	@Column(name = "within_period", nullable = false)
	private int withinPeriod;

	public Policy() {

	}

	public Policy(int id, String name, int property, int withinPeriod) {
		this.id = id;
		this.name = name;
		this.property = property;
		this.withinPeriod = withinPeriod;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProperty() {
		return property;
	}

	public void setProperty(int property) {
		this.property = property;
	}

	public int getWithinPeriod() {
		return withinPeriod;
	}

	public void setWithinPeriod(int withinPeriod) {
		this.withinPeriod = withinPeriod;
	}

}
