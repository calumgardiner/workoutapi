package ii.oo.dto.policy;

public enum Policies {

	MAX_FAILED_LOGIN_ATTEMPTS(new Policy(1, "Max login attempts", 10, 5)), 
	MAX_FAILED_SESSIONS(new Policy(2, "Max failed sessions", 5, 1)),
	MAX_NEW_USERS(new Policy(3, "Max new users", 2, 24));
	
	private Policy policy;

	private Policies(Policy policy) {
		this.policy = policy;
	}

	public Policy getPolicy() {
		return this.policy;
	}
}
