package ii.oo.dto.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "user_password")
public class UserPassword {

	@OneToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "password", nullable = false)
	private String password;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
