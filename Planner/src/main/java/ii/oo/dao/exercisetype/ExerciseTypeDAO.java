package ii.oo.dao.exercisetype;

import java.util.List;

import org.hibernate.Session;

import ii.oo.dto.exercise.ExerciseType;

public class ExerciseTypeDAO implements ExerciseTypeDataAccess {

	private Session session;

	public ExerciseTypeDAO(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseType> getAllExerciseTypes() throws Exception {
		try {
			List<ExerciseType> types = session.createCriteria(ExerciseType.class).list();
			return types;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ExerciseType getExerciseType(int id) throws Exception {
		try {
			ExerciseType type = (ExerciseType) session.get(ExerciseType.class, id);
			return type;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
