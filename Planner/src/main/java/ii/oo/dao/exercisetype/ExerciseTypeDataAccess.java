package ii.oo.dao.exercisetype;

import java.util.List;

import ii.oo.dto.exercise.ExerciseType;

public interface ExerciseTypeDataAccess {

	public List<ExerciseType> getAllExerciseTypes() throws Exception;

	public ExerciseType getExerciseType(int id) throws Exception;

}
