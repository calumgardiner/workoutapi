package ii.oo.dao.utils;

import java.security.SecureRandom;
import java.util.Random;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;

public class SessionUtils {
	
	public static String generateSession() {
		Random random = new SecureRandom();
		byte[] bytes = new byte[32];
		random.nextBytes(bytes);
		return Base64.encodeBase64String(bytes);
	}
	
	public static String hashUserAgent(String userAgent) {
		byte[] bytes = DigestUtils.sha1(userAgent);
		return Base64.encodeBase64String(bytes);
	}

}
