package ii.oo.dao.utils;

import org.mindrot.jbcrypt.BCrypt;

public class PasswordUtils {

	public static String hashPassword(String plainText) {
		String hashedPassword = BCrypt.hashpw(plainText, BCrypt.gensalt());
		return hashedPassword;
	}

	public static Boolean matchesPassword(String hashed, String plainText) {
		return BCrypt.checkpw(plainText, hashed);
	}

	public static Boolean containsNumeric(String password) {
		return password.matches(".*\\d+.*");
	}
}
