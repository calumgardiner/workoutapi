package ii.oo.dao.user;

import java.util.List;

import ii.oo.dto.user.User;
import ii.oo.dto.user.UserPassword;
import ii.oo.flowcontrol.user.UserResponse;

public interface UserDataAccess {

	/**
	 * Get all users.
	 * 
	 * @return all users
	 * @throws Exception
	 */
	public List<User> getAllUsers() throws Exception;

	/**
	 * Get a user by id.
	 * 
	 * @param id
	 * @return user if found else null
	 * @throws Exception
	 */
	public User getUser(int id) throws Exception;

	/**
	 * Save a user object, updates an existing user entry.
	 * 
	 * @param user
	 * @throws Exception
	 */
	public void saveUser(User user) throws Exception;

	/**
	 * Save a new user. Takes a {@link UserPassword} object and firstly saves
	 * the {@link User} object to the database. Then hashes the password and
	 * saves the {@link UserPassword}. Returns the {@link User} object with the
	 * generated id set.
	 * 
	 * @param userPassword
	 * @return {@link User} saved with generated id
	 * @throws Exception
	 */
	public UserResponse saveNewUser(UserPassword userPassword) throws Exception;

	/**
	 * Get a {@link User} by username.
	 * 
	 * @param username
	 * @return {@link User} if found else null
	 * @throws Exception
	 */
	public User getUserByName(String username) throws Exception;

	/**
	 * Validate a login by {@link UserPassword}. The username in the
	 * {@link User} object must be set, the password in the {@link UserPassword}
	 * must be set. Checks that user exists and then checks the password against
	 * the saved hashed value.
	 * 
	 * @param userPassword
	 * @return User object if login validates
	 * @throws Exception
	 */
	public User validateLogin(UserPassword userPassword) throws Exception;
	
}
