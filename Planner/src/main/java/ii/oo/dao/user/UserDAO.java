package ii.oo.dao.user;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ii.oo.dao.utils.PasswordUtils;
import ii.oo.dto.user.User;
import ii.oo.dto.user.UserInfo;
import ii.oo.dto.user.UserPassword;
import ii.oo.flowcontrol.Response;
import ii.oo.flowcontrol.user.UserResponse;

/**
 * Data access for {@link User} and related classes ( {@link UserPassword},
 * {@link UserInfo}).
 * 
 * @author calum
 *
 */
public class UserDAO implements UserDataAccess {

	private Session session;

	public UserDAO(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() throws Exception {
		try {
			List<User> users = session.createCriteria(User.class).list();
			return users;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public User getUser(int id) throws Exception {
		try {
			User user = (User) session.get(User.class, id);
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void saveUser(User user) throws Exception {
		try {
			session.update(user);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public UserResponse saveNewUser(UserPassword userPassword) throws Exception {
		try {
			UserResponse response;
			if (this.getUserByName(userPassword.getUser().getUsername()) != null) {
				response = Response.createResponseWithStatus(UserResponse.class, "That username is already taken");
			}else if (this.getUserByEmail(userPassword.getUser().getEmail()) != null) {
				response = Response.createResponseWithStatus(UserResponse.class, "An account with that email already exists");
			} else {
				// Save user
				int id = (int) session.save(userPassword.getUser());
				// Set the newly generated id
				userPassword.getUser().setId(id);
				// Generate a new salt for this password
				// Hash password
				String hashedPassword = PasswordUtils.hashPassword(userPassword.getPassword());
				// Set hash
				userPassword.setPassword(hashedPassword);
				// Save
				session.save(userPassword);
				response = Response.createOkResponse(UserResponse.class);
				response.setUser(userPassword.getUser());
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public User getUserByName(String username) throws Exception {
		try {
			List<User> users = session.createCriteria(User.class).add(Restrictions.eq("username", username)).list();
			User user = null;
			if (!users.isEmpty()) {
				user = users.get(0);
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	public User getUserByEmail(String email) throws Exception {
		try {
			List<User> users = session.createCriteria(User.class).add(Restrictions.eq("email", email)).list();
			User user = null;
			if (!users.isEmpty()) {
				user = users.get(0);
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public User validateLogin(UserPassword userPassword) throws Exception {
		try {
			Boolean validates = Boolean.FALSE;
			User user = null;
			// search for user password by username
			List<UserPassword> searchDB = session.createCriteria(UserPassword.class).createAlias("user", "user")
					.add(Restrictions.eq("user.username", userPassword.getUser().getUsername())).list();
			UserPassword found = null;
			if (!searchDB.isEmpty()) {
				found = searchDB.get(0);
			}
			// If we found a password by username then check it
			if (found != null) {
				validates = PasswordUtils.matchesPassword(found.getPassword(), userPassword.getPassword());
			}
			if (validates) {
				user = getUserByName(userPassword.getUser().getUsername());
			}
			return user;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
