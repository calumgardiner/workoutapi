package ii.oo.dao.user;

import java.util.List;

import ii.oo.dao.session.SessionDataAccess;
import ii.oo.dto.session.SessionStatus;
import ii.oo.dto.session.UserSession;
import ii.oo.dto.user.User;
import ii.oo.dto.user.UserPassword;
import ii.oo.flowcontrol.user.UserResponse;

public class UserDataAccessSessionDecorator implements UserDataAccess {

	private UserDataAccess userDAO;
	private SessionDataAccess sessionDAO;

	private UserSession userSession;

	public UserDataAccessSessionDecorator(UserDataAccess userDAO, SessionDataAccess sessionDAO,
			UserSession userSession) {
		this.userDAO = userDAO;
		this.sessionDAO = sessionDAO;
		this.userSession = userSession;
	}

	@Override
	public List<User> getAllUsers() throws Exception {
		SessionStatus sessionStatus = sessionDAO.validateSession(userSession);
		if (sessionStatus == SessionStatus.VALID_SESSION) {
			return userDAO.getAllUsers();
		} else {
			throw new Exception(sessionStatus.getDescription());
		}
	}

	@Override
	public User getUser(int id) throws Exception {
		SessionStatus sessionStatus = sessionDAO.validateSession(userSession);
		if (sessionStatus == SessionStatus.VALID_SESSION) {
			return userDAO.getUser(id);
		} else {
			throw new Exception(sessionStatus.getDescription());
		}
	}

	@Override
	public void saveUser(User user) throws Exception {
		SessionStatus sessionStatus = sessionDAO.validateSession(userSession);
		if (sessionStatus == SessionStatus.VALID_SESSION) {
			userDAO.saveUser(user);
		} else {
			throw new Exception(sessionStatus.getDescription());
		}
	}

	@Override
	public UserResponse saveNewUser(UserPassword userPassword) throws Exception {
		// no session checking on new user as people won't have a session at
		// this point
		return userDAO.saveNewUser(userPassword);
	}

	@Override
	public User getUserByName(String username) throws Exception {
		SessionStatus sessionStatus = sessionDAO.validateSession(userSession);
		if (sessionStatus == SessionStatus.VALID_SESSION) {
			return userDAO.getUserByName(username);
		} else {
			throw new Exception(sessionStatus.getDescription());
		}
	}

	@Override
	public User validateLogin(UserPassword userPassword) throws Exception {
		return this.userDAO.validateLogin(userPassword);
	}

}
