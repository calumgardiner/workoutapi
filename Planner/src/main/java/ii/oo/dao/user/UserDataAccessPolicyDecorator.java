package ii.oo.dao.user;

import java.util.List;

import ii.oo.dao.policy.PolicyDataAccess;
import ii.oo.dao.utils.PasswordUtils;
import ii.oo.dto.policy.Policies;
import ii.oo.dto.user.User;
import ii.oo.dto.user.UserPassword;
import ii.oo.flowcontrol.user.UserResponse;

public class UserDataAccessPolicyDecorator implements UserDataAccess {

	private UserDataAccess userDAO;
	private PolicyDataAccess policyDAO;
	private String ipAddress;

	public UserDataAccessPolicyDecorator(UserDataAccess userDAO, PolicyDataAccess policyDAO, String ipAddress) {
		this.userDAO = userDAO;
		this.policyDAO = policyDAO;
		this.ipAddress = ipAddress;
	}

	@Override
	public List<User> getAllUsers() throws Exception {
		return userDAO.getAllUsers();
	}

	@Override
	public User getUser(int id) throws Exception {
		return userDAO.getUser(id);
	}

	@Override
	public void saveUser(User user) throws Exception {
		userDAO.saveUser(user);
	}

	@Override
	public UserResponse saveNewUser(UserPassword userPassword) throws Exception {
		UserResponse response;
		if (policyDAO.confirmPolicy(ipAddress, Policies.MAX_NEW_USERS.getPolicy())) {
			String password = userPassword.getPassword();
			if (password.length() < 6 || !PasswordUtils.containsNumeric(password)) {
				throw new Exception("The password does not meet the password policy");
			}
			response = userDAO.saveNewUser(userPassword);
			policyDAO.violatePolicy(ipAddress, Policies.MAX_NEW_USERS.getPolicy());
		} else {
			response = new UserResponse();
			response.setStatus("Maximum number of new users from this address");
		}
		return response;
	}

	@Override
	public User getUserByName(String username) throws Exception {
		return userDAO.getUserByName(username);
	}

	@Override
	public User validateLogin(UserPassword userPassword) throws Exception {
		if (policyDAO.confirmPolicy(ipAddress, Policies.MAX_FAILED_LOGIN_ATTEMPTS.getPolicy())) {
			User validates = userDAO.validateLogin(userPassword);
			if (validates == null) {
				policyDAO.violatePolicy(ipAddress, Policies.MAX_FAILED_LOGIN_ATTEMPTS.getPolicy());
			}
			return validates;
		} else {
			throw new Exception("Maximum failed login attempts reached");
		}
	}

}
