package ii.oo.dao.policy;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ii.oo.dto.policy.Policies;
import ii.oo.dto.policy.Policy;
import ii.oo.dto.policy.PolicyViolation;

public class PolicyDAO implements PolicyDataAccess {

	private Session session;
	private Logger log = Logger.getLogger(this.getClass().getName());

	public PolicyDAO(Session session) {
		this.session = session;
	}

	@Override
	public Policy confirmAllPolicies(String ip) throws Exception {
		Policy violatedPolicy = null;
		log.info("Confirming all policies");
		for (Policies p : Policies.values()) {
			if (!confirmPolicy(ip, p.getPolicy())) {
				violatedPolicy = p.getPolicy();
				break;
			}
		}
		return violatedPolicy;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Boolean confirmPolicy(String ip, Policy policy) throws Exception {
		try {
			log.info(String.format("Confirming polciy %s for IP %s", policy.getName(), ip));
			Boolean confirmed = Boolean.TRUE;
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.HOUR_OF_DAY, (policy.getWithinPeriod() * -1));
			Date previousPolicyPeriod = cal.getTime();
			List<PolicyViolation> violationsInPeriod = session.createCriteria(PolicyViolation.class)
					.add(Restrictions.eq("ipAddress", ip)).add(Restrictions.gt("time", previousPolicyPeriod)).list();
			if (violationsInPeriod.size() > policy.getProperty()) {
				confirmed = Boolean.FALSE;
			}
			return confirmed;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public void violatePolicy(String ip, Policy policy) throws Exception {
		try {
			log.info(String.format("Setting IP %s as violating policy %s", ip, policy.getName()));
			PolicyViolation violation = new PolicyViolation();
			violation.setIpAddress(ip);
			violation.setPolicy(policy);
			violation.setTime(new Date());
			session.save(violation);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
