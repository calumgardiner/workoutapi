package ii.oo.dao.policy;

import ii.oo.dto.policy.Policy;

public interface PolicyDataAccess {

	public Policy confirmAllPolicies(String ip) throws Exception;

	public Boolean confirmPolicy(String ip, Policy policy) throws Exception;

	public void violatePolicy(String ip, Policy policy) throws Exception;

}
