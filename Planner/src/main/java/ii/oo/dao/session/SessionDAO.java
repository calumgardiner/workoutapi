package ii.oo.dao.session;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ii.oo.dao.utils.SessionUtils;
import ii.oo.dto.session.SessionExpiry;
import ii.oo.dto.session.SessionStatus;
import ii.oo.dto.session.UserSession;
import ii.oo.dto.user.User;

public class SessionDAO implements SessionDataAccess {

	private Session session;
	private Logger log = Logger.getLogger(this.getClass().getName());

	public SessionDAO(Session session) {
		this.session = session;
	}
	
	public UserSession createUserSession(User user, String ip, String userAgent) throws Exception {
		try {
			log.info("Creating session for user " + user.getUsername());
			UserSession userSession = new UserSession();
			userSession.setUser(user);
			userSession.setIp(ip);
			userSession.setSessionStart(new Date());
			userSession.setUserAgent(SessionUtils.hashUserAgent(userAgent));
			while (userSession.getSessionId() == null || !isUniqueSession(userSession.getSessionId())) {
				userSession.setSessionId(SessionUtils.generateSession());
			}
			int id = (int) session.save(userSession);
			userSession.setId(id);
			return userSession;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	@SuppressWarnings("unchecked")
	private boolean isUniqueSession(String sessionId) throws Exception {
		try {
			List<UserSession> sessionsFromDB = session.createCriteria(UserSession.class)
					.add(Restrictions.eq("sessionId", sessionId)).list();
			return sessionsFromDB.isEmpty();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	public SessionStatus validateSession(UserSession userSession) throws Exception {
		log.info(String.format("Validation Session %s %s %s %s", userSession.getId(), userSession.getSessionId(),
				userSession.getIp(), userSession.getUserAgent()));
		SessionStatus status = SessionStatus.INVALID_SESSION;
		try {
			List<UserSession> sessionsFromDB = session.createCriteria(UserSession.class)
					.add(Restrictions.eq("sessionId", userSession.getSessionId())).list();
			if (!sessionsFromDB.isEmpty()) {
				UserSession fromDB = sessionsFromDB.get(0);
				if (!fromDB.getIp().equals(userSession.getIp())) {
					status = SessionStatus.INVALID_SESSION;
				} else if (fromDB.getUserAgent().equals(userSession.getUserAgent())) {
					status = SessionStatus.INVALID_SESSION;
				} else if (fromDB.getSessionStart().after(getSessionExpiry())) {
					status = SessionStatus.SESSION_EXPIRED;
				} else {
					status = SessionStatus.VALID_SESSION;
				}
			}
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	private Date getSessionExpiry() throws Exception {
		try {
			Calendar cal = Calendar.getInstance();
			List<SessionExpiry> expirySearch = session.createCriteria(SessionExpiry.class).list();
			SessionExpiry expiry = expirySearch.get(0);
			cal.add(Calendar.HOUR, expiry.getHoursToExpiry());
			return cal.getTime();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
