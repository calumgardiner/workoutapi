package ii.oo.dao.session;

import ii.oo.dto.session.SessionStatus;
import ii.oo.dto.session.UserSession;
import ii.oo.dto.user.User;

public interface SessionDataAccess {

	public SessionStatus validateSession(UserSession session) throws Exception;
	
	public UserSession createUserSession(User user, String ip, String userAgent) throws Exception;

}
