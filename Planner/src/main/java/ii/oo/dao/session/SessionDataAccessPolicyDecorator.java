package ii.oo.dao.session;

import ii.oo.dao.policy.PolicyDataAccess;
import ii.oo.dto.policy.Policies;
import ii.oo.dto.session.SessionStatus;
import ii.oo.dto.session.UserSession;
import ii.oo.dto.user.User;

public class SessionDataAccessPolicyDecorator implements SessionDataAccess {

	private SessionDataAccess sessionDAO;
	private PolicyDataAccess policyDAO;

	public SessionDataAccessPolicyDecorator(SessionDataAccess sessionDAO, PolicyDataAccess policyDAO) {
		this.sessionDAO = sessionDAO;
		this.policyDAO = policyDAO;
	}

	@Override
	public SessionStatus validateSession(UserSession session) throws Exception {
		if (policyDAO.confirmPolicy(session.getIp(), Policies.MAX_FAILED_SESSIONS.getPolicy())) {
			SessionStatus status = sessionDAO.validateSession(session);
			if (status == SessionStatus.INVALID_SESSION) {
				policyDAO.violatePolicy(session.getIp(), Policies.MAX_FAILED_SESSIONS.getPolicy());
			}
			return status;
		} else {
			throw new Exception("Maximum failed session attempts reached");
		}
	}

	@Override
	public UserSession createUserSession(User user, String ip, String userAgent) throws Exception {
		return this.sessionDAO.createUserSession(user, ip, userAgent);
	}

}
