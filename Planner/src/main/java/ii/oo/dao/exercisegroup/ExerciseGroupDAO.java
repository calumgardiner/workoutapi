package ii.oo.dao.exercisegroup;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ii.oo.dto.exercise.ExerciseGroup;

public class ExerciseGroupDAO implements ExerciseGroupDataAccess {

	private Session session;

	public ExerciseGroupDAO(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	public List<ExerciseGroup> getAllExerciseGroups() throws Exception {
		try {
			List<ExerciseGroup> groups = session.createCriteria(ExerciseGroup.class).list();
			return groups;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public ExerciseGroup getExerciseGroup(int id) throws Exception {
		try {
			ExerciseGroup group = (ExerciseGroup) session.get(ExerciseGroup.class, id);
			return group;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public ExerciseGroup getLiftGroup() throws Exception {
		try {
			List<ExerciseGroup> groups = session.createCriteria(ExerciseGroup.class)
					.add(Restrictions.eq("name", "Lift")).list();
			ExerciseGroup group = null;
			if (!groups.isEmpty()) {
				group = groups.get(0);
			}
			return group;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public ExerciseGroup getCardioGroup() throws Exception {
		try {
			List<ExerciseGroup> groups = session.createCriteria(ExerciseGroup.class)
					.add(Restrictions.eq("name", "Cardio")).list();
			ExerciseGroup group = null;
			if (!groups.isEmpty()) {
				group = groups.get(0);
			}
			return group;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
