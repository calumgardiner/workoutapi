package ii.oo.dao.exercisegroup;

import java.util.List;

import ii.oo.dto.exercise.ExerciseGroup;

public interface ExerciseGroupDataAccess {

	public List<ExerciseGroup> getAllExerciseGroups() throws Exception;

	public ExerciseGroup getExerciseGroup(int id) throws Exception;

	public ExerciseGroup getLiftGroup() throws Exception;

	public ExerciseGroup getCardioGroup() throws Exception;
}
