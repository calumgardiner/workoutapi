package ii.oo.dao.security;

import org.hibernate.Session;

import ii.oo.dao.authorization.AuthorizationDAO;
import ii.oo.dao.authorization.AuthorizationDataAccess;
import ii.oo.dao.exercise.ExerciseDAO;
import ii.oo.dao.exercise.ExerciseDataAccess;
import ii.oo.dao.exercisegroup.ExerciseGroupDAO;
import ii.oo.dao.exercisegroup.ExerciseGroupDataAccess;
import ii.oo.dao.exercisetype.ExerciseTypeDAO;
import ii.oo.dao.exercisetype.ExerciseTypeDataAccess;
import ii.oo.dao.lift.LiftDAO;
import ii.oo.dao.lift.LiftDataAccess;
import ii.oo.dao.policy.PolicyDAO;
import ii.oo.dao.policy.PolicyDataAccess;
import ii.oo.dao.session.SessionDAO;
import ii.oo.dao.session.SessionDataAccess;
import ii.oo.dao.session.SessionDataAccessPolicyDecorator;
import ii.oo.dao.user.UserDAO;
import ii.oo.dao.user.UserDataAccess;
import ii.oo.dao.user.UserDataAccessPolicyDecorator;
import ii.oo.dao.user.UserDataAccessSessionDecorator;
import ii.oo.dto.session.UserSession;

public class SecurityDAOProvider implements DAOProvider {

	private UserSession userSession;

	public SecurityDAOProvider(UserSession userSession) {
		this.userSession = userSession;
	}

	@Override
	public AuthorizationDataAccess getAuthorizationDAO(Session session) {
		return new AuthorizationDAO(session);
	}

	@Override
	public ExerciseDataAccess getExerciseDAO(Session session) {
		ExerciseDataAccess exerciseDAO = new ExerciseDAO(session);
		return exerciseDAO;
	}

	@Override
	public ExerciseGroupDataAccess getExerciseGroupDAO(Session session) {
		ExerciseGroupDataAccess exerciseGroupDAO = new ExerciseGroupDAO(session);
		return exerciseGroupDAO;
	}

	@Override
	public ExerciseTypeDataAccess getExerciseTypeDAO(Session session) {
		ExerciseTypeDataAccess exerciseTypeDAO = new ExerciseTypeDAO(session);
		return exerciseTypeDAO;
	}

	@Override
	public LiftDataAccess getLiftDAO(Session session) {
		LiftDataAccess liftDAO = new LiftDAO(session);
		return liftDAO;
	}

	@Override
	public SessionDataAccess getSessionDAO(Session session) {
		SessionDataAccess sessionDAO = new SessionDAO(session);
		sessionDAO = new SessionDataAccessPolicyDecorator(sessionDAO, getPolicyDAO(session));
		return sessionDAO;
	}

	@Override
	public UserDataAccess getUserDAO(Session session) {
		UserDataAccess userDAO = new UserDAO(session);
		userDAO = new UserDataAccessPolicyDecorator(userDAO, getPolicyDAO(session), userSession.getIp());
		userDAO = new UserDataAccessSessionDecorator(userDAO, getSessionDAO(session), userSession);
		return userDAO;
	}

	@Override
	public PolicyDataAccess getPolicyDAO(Session session) {
		return new PolicyDAO(session);
	}

}
