package ii.oo.dao.security;

import org.hibernate.Session;

import ii.oo.dao.authorization.AuthorizationDataAccess;
import ii.oo.dao.exercise.ExerciseDataAccess;
import ii.oo.dao.exercisegroup.ExerciseGroupDataAccess;
import ii.oo.dao.exercisetype.ExerciseTypeDataAccess;
import ii.oo.dao.lift.LiftDataAccess;
import ii.oo.dao.policy.PolicyDataAccess;
import ii.oo.dao.session.SessionDataAccess;
import ii.oo.dao.user.UserDataAccess;

public interface DAOProvider {

	public AuthorizationDataAccess getAuthorizationDAO(Session session);

	public ExerciseDataAccess getExerciseDAO(Session session);

	public ExerciseGroupDataAccess getExerciseGroupDAO(Session session);

	public ExerciseTypeDataAccess getExerciseTypeDAO(Session session);

	public LiftDataAccess getLiftDAO(Session session);

	public SessionDataAccess getSessionDAO(Session session);

	public UserDataAccess getUserDAO(Session session);
	
	public PolicyDataAccess getPolicyDAO(Session session);

}
