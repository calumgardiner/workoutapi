package ii.oo.dao.exercise;

import java.util.List;

import ii.oo.dto.exercise.Exercise;
import ii.oo.dto.exercise.ExerciseGroup;

public interface ExerciseDataAccess {
	
	public List<Exercise> getAllExercises() throws Exception;

	public Exercise getExercise(int id) throws Exception;

	public List<Exercise> getAllExercises(ExerciseGroup group) throws Exception;

}
