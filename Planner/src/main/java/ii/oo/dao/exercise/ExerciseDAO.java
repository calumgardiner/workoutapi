package ii.oo.dao.exercise;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import ii.oo.dto.exercise.Exercise;
import ii.oo.dto.exercise.ExerciseGroup;

public class ExerciseDAO implements ExerciseDataAccess {

	private Session session;

	public ExerciseDAO(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	public List<Exercise> getAllExercises() throws Exception {
		try {
			List<Exercise> exercises = session.createCriteria(Exercise.class).list();
			return exercises;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Exercise getExercise(int id) throws Exception {
		try {
			Exercise exercise = (Exercise) session.get(Exercise.class, id);
			return exercise;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@SuppressWarnings("unchecked")
	public List<Exercise> getAllExercises(ExerciseGroup group) throws Exception {
		try {
			List<Exercise> exercises = session.createCriteria(Exercise.class).createAlias("exerciseType", "type")
					.add(Restrictions.eq("type.group", group)).list();
			return exercises;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

}
