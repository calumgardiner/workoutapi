package ii.oo.dao.authorization;

import org.hibernate.Session;

import ii.oo.dto.authorization.Action;
import ii.oo.dto.authorization.AuthorizationStatus;
import ii.oo.dto.user.User;

public class AuthorizationDAO implements AuthorizationDataAccess {

	private Session session;

	public AuthorizationDAO(Session session) {
		this.session = session;
	}

	public AuthorizationStatus checkUserIsAuthorized(User user, User targetUser, Action action) throws Exception {
		this.session.beginTransaction();
		return AuthorizationStatus.AUTHORIZED;
	}
}
