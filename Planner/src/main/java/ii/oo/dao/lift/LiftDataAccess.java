package ii.oo.dao.lift;

import java.util.List;

import ii.oo.dto.exercise.Lift;

public interface LiftDataAccess {

	public List<Lift> getAllLifts() throws Exception;

	public Lift getLift(int id) throws Exception;
}
