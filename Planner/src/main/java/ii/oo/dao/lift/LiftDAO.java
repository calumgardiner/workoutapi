package ii.oo.dao.lift;

import java.util.List;

import org.hibernate.Session;

import ii.oo.dto.exercise.Lift;

public class LiftDAO implements LiftDataAccess {
	
	private Session session;

	public LiftDAO(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	public List<Lift> getAllLifts() throws Exception {
		try {
			List<Lift> lifts = session.createCriteria(Lift.class).list();
			return lifts;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public Lift getLift(int id) throws Exception {
		try {
			Lift lift = (Lift) session.get(Lift.class, id);
			return lift;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
}
